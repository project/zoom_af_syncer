<?php

/**
 * @file
 * Drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function zoom_af_syncer_drush_command() {
  $items['run_zoom_af_syncer'] = [
    'description' => 'Run Openy AF Zoom sync.',
    'aliases' => ['sync-zoom'],
    'callback' => 'zoom_af_syncer_callback',
    'drupal dependencies' => ['zoom_af_syncer'],
    'examples' => [
      'drush sync-zoom' => 'Start AF Zoom synchronization.',
    ],
  ];
  return $items;
}

/**
 * Drush command callback.
 */
function zoom_af_syncer_callback() {
  try {
    ymca_sync_run("zoom_af_syncer.syncer", "proceed");
  }
  catch (\Exception $e) {
    watchdog_exception('zoom_af_syncer', $e);
  }
}
