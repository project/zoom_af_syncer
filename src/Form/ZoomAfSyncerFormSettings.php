<?php

namespace Drupal\zoom_af_syncer\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\openy_mappings\LocationMappingRepository;

/**
 * Configure OpenY PEF GXP Sync settings for this site.
 */
class ZoomAfSyncerFormSettings extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Mapping repository.
   *
   * @var \Drupal\ymca_mappings\LocationMappingRepository
   */
  protected $mappingRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\openy_mappings\LocationMappingRepository $mappingRepository
   *   Location mapping repo.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, LocationMappingRepository $mappingRepository) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->mappingRepository = $mappingRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('openy_mappings.location_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zoom_af_syncer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['zoom_af_syncer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('zoom_af_syncer.settings');
    $locationsMapping = $config->get('locations_mapping');
    $trackingFieldsMapping = $config->get('tracking_fields_mapping');
    $allLocations = $config->get('all_locations');

    $location_ids = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', 'branch')
      ->condition('status', 1)
      ->execute();

    if (!$location_ids) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage('node');
    $locationsList = $storage->loadMultiple($location_ids);

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this
        ->t('This configuration form provides settings of Zoom meetings integration with Activity Finder and defines integration mapping for Zoom Tracking fileds and Recommended values of Location field.'),
    ];

    $form['data_service_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Data Service Url'),
      '#default_value' => $config->get('data_service_url'),
      '#description' => $this->t('Data Service Url'),
    ];

    $form['data_service_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data Service Token'),
      '#default_value' => $config->get('data_service_token'),
      '#description' => $this->t('Data Service Token'),
      '#maxlength' => 1000,
    ];

    $form['tracking_fields_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tracking Fields Mapping Settings'),
    ];
    $form['tracking_fields_mapping']['help_text'] = [
      '#type' => 'markup',
      '#markup' => $this
        ->t('Please specify the exact names of Tracking fileds in your Zoom account
<a target=_blank href="https://zoom.us/account/track/field">https://zoom.us/account/track/field</a>'),
    ];
    $form['tracking_fields_mapping']['displayInAf'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display in the Activity Finder'),
      '#default_value' => $trackingFieldsMapping['displayInAf'],
    ];
    $form['tracking_fields_mapping']['Location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#default_value' => $trackingFieldsMapping['Location'],
    ];
    $form['tracking_fields_mapping']['Category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category'),
      '#default_value' => $trackingFieldsMapping['Category'],
    ];
    $form['tracking_fields_mapping']['SubCategory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub-Category'),
      '#default_value' => $trackingFieldsMapping['SubCategory'],
    ];
    $form['tracking_fields_mapping']['Instructor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instructor'),
      '#default_value' => $trackingFieldsMapping['Instructor'],
    ];
    $form['all_locations'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings for "All locations" meetings'),
      '#description' => $this->t('Please specify the name of All locations option in Zoom Location Tracking field'),
    ];

    $form['all_locations']['all_locations_option_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name for All locations option'),
      '#default_value' => $allLocations['option_name'],
    ];

    $form['all_locations']['all_locations_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable generation of sessions for all locations if "All locations" option selected in the Location Tracking field'),
      '#default_value' => $allLocations['enbaled'],
    ];

    $form['locations_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Zoom Locations mapping'),
    ];
    $form['locations_mapping']['help_text'] = [
      '#type' => 'markup',
      '#markup' => $this
        ->t('Please specify the Recommended values of Location Tracking field in Zoom'),
    ];

    // Build options list.
    foreach ($locationsMapping as $value) {
      $id = $value['branch_id'];
      $node = $locationsList[$id];
      if (empty($node)) {
        continue;
      }
      $form['locations_mapping'][$id] = [
        '#type' => 'textfield',
        '#title' => $node->label(),
        '#default_value' => $value['branch_name'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the config.
    $config = $this->config('zoom_af_syncer.settings');
    $locationsMapping = [];

    foreach ($form['locations_mapping'] as $locationId => $form_item) {
      if (!is_int($locationId)) {
        continue;
      }
      $locationsMapping[] = [
        'branch_id' => $locationId,
        'branch_name' => $form_state->getValue($locationId),
      ];
    }

    $trackingFieldsMapping = [
      'displayInAf' => $form_state->getValue('displayInAf'),
      'Location' => $form_state->getValue('Location'),
      'SubCategory' => $form_state->getValue('SubCategory'),
      'Category' => $form_state->getValue('Category'),
      'Instructor' => $form_state->getValue('Instructor'),
    ];

    $allLocations = [
      'option_name' => $form_state->getValue('all_locations_option_name'),
      'enbaled' => $form_state->getValue('all_locations_enabled'),
    ];

    $config->set('all_locations', $allLocations);
    $config->set('data_service_token', $form_state->getValue('data_service_token'));
    $config->set('data_service_url', $form_state->getValue('data_service_url'));
    $config->set('tracking_fields_mapping', $trackingFieldsMapping);
    $config->set('locations_mapping', $locationsMapping);
    $config->save();
    $this->configFactory()->reset('zoom_af_syncer.settings');
    parent::submitForm($form, $form_state);
  }

}
