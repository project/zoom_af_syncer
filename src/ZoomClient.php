<?php

namespace Drupal\zoom_af_syncer;

use Drupal\Core\Config\ImmutableConfig;

/**
 * Class ZoomClient.
 *
 * @package Drupal\zoom_af_sync
 */
class ZoomClient {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ClientFactory constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Config.
   */
  public function __construct(ImmutableConfig $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function get($requestPath) {
    $baseUrl = $this->config->get('data_service_url');
    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => $baseUrl . $requestPath,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        'authorization: Bearer ' . $this->config->get('data_service_token'),
        'content-type: application/json',
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    return ['response' => $response, 'errors' => $err];
  }

}
