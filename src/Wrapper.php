<?php

namespace Drupal\zoom_af_syncer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\openy_af_personify_sync\syncer\WrapperInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Class Wrapper.
 *
 * @package Drupal\zoom_af_syncer.
 */
class Wrapper implements WrapperInterface {

  /**
   * Name of the syncer.
   */
  const SYNC_ID = 'zoom_af_sync';

  // All locations option.
  const ALL_LOCATIONS = 'All locations';
  /**
   * Code for weekly reccurrency.
   */
  const ZOOM_WEEKLY_REPEAT_INTERVAL = 1;

  /**
   * Reccurrency type for weekly.
   */
  const ZOOM_WEEKLY_TYPE = 2;

  /**
   * Meetings from zoom.
   *
   * @var array
   */
  protected $products;

  /**
   * EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  public $config;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  public $logger;

  /**
   * Locations mapping.
   *
   * @var array
   */
  public $locationMappings;

  /**
   * Name of tracking_fields.
   *
   * @var array
   */
  public $dynamicFields;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type manager.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Config.
   */
  public function __construct(LoggerChannelInterface $logger, EntityTypeManagerInterface $entityTypeManager, ImmutableConfig $config) {
    $this->products = [];
    $this->logger = $logger;
    $this->config = $config;
    $this->locationMappings = $this->getLocationsMapping();
    $this->dynamicFields = $this->config->get('tracking_fields_mapping');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function addProducts(array $products) {
    $this->logger->notice('Start enrich zoom meetings');
    $dynamicFields = $this->dynamicFields;
    $enrichedProducts = [];
    $allLocations = $this->config->get('all_locations');

    foreach ($products as $meeting) {
      // Process with tracking fields.
      if (!isset($meeting->tracking_fields)) {
        continue;
      }
      $fields = [];
      foreach ($meeting->tracking_fields as $field) {
        $fields[$field->field] = $field->value;
      }
      if (!$this->checkDynamicFields($fields, $meeting)) {
        continue;
      }

      // Case for all locations.
      if (
      isset($fields[$dynamicFields['Location']])
      && $fields[$dynamicFields['Location']] == $allLocations['option_name']
      && $allLocations['enbaled']
      ) {
        $newMettings = $this->createAllLocations($meeting);
        foreach ($newMettings as $uid => $newMeeting) {
          $enrichedProducts[$uid] = $newMeeting;
        }
        continue;
      }

      if (!isset($this->locationMappings[$fields[$dynamicFields['Location']]])) {
        $msg = 'Meeting "%id - %topic" have not expected location title "%title", continue.';
        $this->logger->info($msg, [
          '%id' => $meeting->id,
          '%topic' => $meeting->topic,
          '%title' => $fields[$dynamicFields['Location']],
        ]);
        continue;
      }

      // Set node id of location for session.
      $fields['LocationId'] = $this->locationMappings[$fields[$dynamicFields['Location']]];

      // Convert array fields to object.
      $meeting->tracking_fields = (object) $fields;

      // Process with meeting.
      if (!isset($meeting->start_time) && !isset($meeting->occurrences)) {
        $msg = 'Meeting "%id - %topic" have not field start_time and occurrences, continue.';
        $this->logger->info($msg, ['%id' => $meeting->id, '%topic' => $meeting->topic]);
        continue;
      }
      // Convert meeting with undefined recurrence to simple meeting.
      if (
      isset($meeting->recurrence)
      && ($meeting->recurrence->type != self::ZOOM_WEEKLY_TYPE
      || $meeting->recurrence->repeat_interval != self::ZOOM_WEEKLY_REPEAT_INTERVAL)) {
        $meeting = $this->simpleMeetingConvert($meeting);
        // Enrich meeting to Personify product.
        $enrichedProduct = $this->enrichProduct($meeting);
        $enrichedProducts[substr($meeting->id, 2)] = $enrichedProduct;
        continue;
      }
      // Enrich meeting to Personify product.
      $enrichedProduct = $this->enrichProduct($meeting);
      $enrichedProducts[substr($meeting->id, 2)] = $enrichedProduct;
    }
    $msg = 'Finished enrich meetings. Count zoom meetings to save: %count';
    $this->logger->notice($msg, ['%count' => count($enrichedProducts)]);
    $this->products = $enrichedProducts;

  }

  /**
   * Convert zoom meeting to product.
   *
   * @param object $meeting
   *   Zoom meeting object.
   *
   * @return object
   *   Product;
   */
  private function enrichProduct(object $meeting) {
    $startDate = NULL;
    $endDate = NULL;
    if (isset($meeting->start_time)) {
      $startDate = new \DateTime($meeting->start_time, new \DateTimeZone('UTC'));
      $endDate = new \DateTime($meeting->start_time, new \DateTimeZone('UTC'));
      $endDate->modify('+' . $meeting->duration . ' minutes');
    }
    if (isset($meeting->occurrences)
    && isset($meeting->recurrence)
    && $meeting->recurrence->repeat_interval == self::ZOOM_WEEKLY_REPEAT_INTERVAL) {
      $startDate = new \DateTime($meeting->occurrences[0]->start_time, new \DateTimeZone('UTC'));
      $endOcurrence = end($meeting->occurrences);
      $endDate = new \DateTime($endOcurrence->start_time, new \DateTimeZone('UTC'));
      $endDate->modify('+' . $endOcurrence->duration . ' minutes');
    }
    $instructor = '';
    if (isset($meeting->tracking_fields->{$this->dynamicFields['Instructor']}) && !empty($meeting->tracking_fields->{$this->dynamicFields['Instructor']})) {
      $instructor = trim($meeting->tracking_fields->{$this->dynamicFields['Instructor']});
    }
    $productAf = [
      'ProductId' => (int) substr($meeting->id, 2),
      'ProductCode' => 'Zoom_' . $meeting->id,
      'ShortName' => $meeting->topic,
      'WebDisplayStartDate' => $startDate,
      'WebDisplayEndDate' => $endDate,
      'Availability' => '9999999',
      'ScheduleOverride' => '',
      'BranchId' => $meeting->tracking_fields->LocationId,
      'FacilityName' => $meeting->tracking_fields->{$this->dynamicFields['Location']},
      'ShortDescription' => isset($meeting->agenda) ? $meeting->agenda : NULL,
      'UserMinYears' => '55',
      'UserMaxYears' => '99',
      'UserMinMonths' => '',
      'UserMaxMonths' => '',
      'UserGender' => 'coed',
      'WaitListCapacity' => '',
      'WaitListRegistrations' => '',
      'programTitle' => $meeting->tracking_fields->{$this->dynamicFields['Category']},
      'programSubcategoryTitle' => $meeting->tracking_fields->{$this->dynamicFields['SubCategory']},
      'classTitle' => 'Class' . $meeting->tracking_fields->{$this->dynamicFields['Category']} . '_' . $meeting->tracking_fields->{$this->dynamicFields['SubCategory']},
      'regLink' => isset($meeting->registration_url) ? $meeting->registration_url : $meeting->join_url,
      'Instructor' => $instructor,
      'prices' => [['c' => 'STANDARD', 'mbronly' => 'N']],
    ];

    $weekFlags = $this->weekDays($meeting);
    $productAf = array_merge($productAf, $weekFlags);
    $productAf['meta']['hash'] = md5(serialize($productAf));
    return (object) $productAf;
  }

  /**
   * Convert recurrence to Personify week flags.
   *
   * @param object $meeting
   *   Zoom meeting object.
   *
   * @return array
   *   Flags.
   */
  private function weekDays(object $meeting) {
    // @TODO implement config for week day mapping.
    $days = [
      2 => 'MondayFlag',
      3 => 'TuesdayFlag',
      4 => 'WednesdayFlag',
      5 => 'ThursdayFlag',
      6 => 'FridayFlag',
      7 => 'SaturdayFlag',
      1 => 'SundayFlag',
    ];
    $flags = [
      'MondayFlag' => 'N',
      'TuesdayFlag' => 'N',
      'WednesdayFlag' => 'N',
      'ThursdayFlag' => 'N',
      'FridayFlag' => 'N',
      'SaturdayFlag' => 'N',
      'SundayFlag' => 'N',
    ];
    if (isset($meeting->recurrence)) {
      $recurrenceDays = explode(',', $meeting->recurrence->weekly_days);
      foreach ($days as $index => $day) {
        if (in_array($index, $recurrenceDays)) {
          $flags[$day] = 'Y';
        }
      }
    }
    if (isset($meeting->start_time)) {
      $timeZone = new \DateTimeZone(date_default_timezone_get());
      $date = new \DateTime($meeting->start_time, new \DateTimeZone('UTC'));
      $date->setTimezone($timeZone);
      // Zoom has week numbers started from 1-Sunday, and PHP: 0-Sunday.
      $week = (int) $date->format("w") + 1;
      foreach ($days as $index => $day) {
        if ($index == $week) {
          $flags[$day] = 'Y';
        }
      }
    }
    return $flags;
  }

  /**
   * {@inheritdoc}
   */
  public function getProducts() {
    return $this->products;
  }

  /**
   * {@inheritdoc}
   */
  public function getFacilities() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function addFacilities(array $facilities) {
    // Pass this method.
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncId() {
    return self::SYNC_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocation(object $product) {
    $location = $this->entityTypeManager->getStorage('node')->load($product->BranchId);
    if (!$location) {
      // @TODO log messages.
      return NULL;
    }
    return $location;
  }

  /**
   * Convert meeting with undefined recurrence to simple meeting.
   *
   * @param object $meeting
   *   Meeting object.
   *
   * @return object
   *   Meeting object.
   */
  private function simpleMeetingConvert(object $meeting) {
    $rebaseDataByOccurrence = (array) $meeting;
    $rebaseDataByOccurrence['start_time'] = $meeting->occurrences[0]->start_time;
    $rebaseDataByOccurrence['duration'] = $meeting->occurrences[0]->duration;
    unset($rebaseDataByOccurrence['occurrences']);
    unset($rebaseDataByOccurrence['recurrence']);
    return (object) $rebaseDataByOccurrence;
  }

  /**
   * Check Dynamic fields.
   *
   * @param array $fields
   *   Tracking_fields.
   * @param object $meeting
   *   Meeting object.
   *
   * @return bool
   *   Check fields.
   */
  private function checkDynamicFields(array $fields, object $meeting) {
    if (!isset($fields[$this->dynamicFields['displayInAf']])) {
      return FALSE;
    }
    if ($fields[$this->dynamicFields['displayInAf']] != 'Yes') {
      return FALSE;
    }
    if (!isset($fields[$this->dynamicFields['Location']]) || empty($fields[$this->dynamicFields['Location']])) {
      $msg = 'Meeting "%id - %topic" not has field Location or field empty, continue.';
      $this->logger->info($msg, ['%id' => $meeting->id, '%topic' => $meeting->topic]);
      return FALSE;
    }
    if (!isset($fields[$this->dynamicFields['SubCategory']]) || empty($fields[$this->dynamicFields['SubCategory']])) {
      $msg = 'Meeting "%id - %topic" not has field Sub-Category or field empty, continue.';
      $this->logger->info($msg, ['%id' => $meeting->id, '%topic' => $meeting->topic]);
      return FALSE;
    }
    if (!isset($fields[$this->dynamicFields['Category']]) || empty($fields[$this->dynamicFields['Category']])) {
      $msg = 'Meeting "%id - %topic" not has field Category or field empty, continue.';
      $this->logger->info($msg, ['%id' => $meeting->id, '%topic' => $meeting->topic]);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get locations mapping.
   *
   * @return array
   *   Locations mapping.
   */
  private function getLocationsMapping() {
    $locationMappingConfig = $this->config->get('locations_mapping');
    $locationMapping = [];
    foreach ($locationMappingConfig as $location) {
      if (!isset($location['branch_id'])) {
        continue;
      }
      if (!isset($location['branch_name'])) {
        continue;
      }
      $locationMapping[$location['branch_name']] = $location['branch_id'];
    }
    return (array) $locationMapping;
  }

  /**
   * Generate meeting for all location.
   *
   * @param object $meeting
   *   Parameter object.
   *
   * @return array
   *   Array of products.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  private function createAllLocations(object $meeting) {
    $meetings = [];
    $location_ids = $this->entityTypeManager->getStorage('node')
      ->getQuery()
      ->condition('type', 'branch')
      ->condition('status', 1)
      ->execute();
    $storage = $this->entityTypeManager->getStorage('node');
    $locationsList = $storage->loadMultiple($location_ids);
    $count = 0;
    foreach ($locationsList as $location) {
      $uid = (int) substr($meeting->id, 2) + $count;
      $newMeeting = clone $meeting;
      $newMeeting->id = $uid;
      $fields = [];
      foreach ($newMeeting->tracking_fields as $field) {
        $fields[$field->field] = $field->value;
      }
      $fields['LocationId'] = $location->id();
      $fields[$this->dynamicFields['Location']] = $location->label();
      $newMeeting->tracking_fields = (object) $fields;
      $enrichedProduct = $this->enrichProduct($newMeeting);
      $meetings[$uid] = $enrichedProduct;
      $count++;
    }
    return $meetings;
  }

}
