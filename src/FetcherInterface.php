<?php

namespace Drupal\zoom_af_syncer;

/**
 * Interface FetcherInterface.
 *
 * @package Drupal\zoom_af_syncer
 */
interface FetcherInterface {

  /**
   * Fetch zoom meetings.
   */
  public function fetch();

}
