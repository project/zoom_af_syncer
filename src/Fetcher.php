<?php

namespace Drupal\zoom_af_syncer;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\openy_af_personify_sync\SyncException;

/**
 * Class Fetcher.
 *
 * @package Drupal\zoom_af_sync.
 */
class Fetcher implements FetcherInterface {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Client.
   *
   * @var \Drupal\zoom_af_syncer\ZoomClient
   */
  protected $client;

  /**
   * Wrapper.
   *
   * @var \Drupal\zoom_af_syncer\Wrapper
   */
  protected $wrapper;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Config.
   * @param \Drupal\zoom_af_syncer\ZoomClient $client
   *   Client.
   * @param \Drupal\zoom_af_syncer\Wrapper $wrapper
   *   Wrapper.
   */
  public function __construct(ImmutableConfig $config, ZoomClient $client, Wrapper $wrapper) {
    $this->config = $config;
    $this->client = $client;
    $this->wrapper = $wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch() {
    $this->wrapper->logger->notice('Start zoom af sync fetch');

    // Get all users of account.
    $requestUsers = '/users?page_size=1000&page_number=1';
    $body = $this->client->get($requestUsers);
    $parseUsers = $this->parse($body);
    if (!$parseUsers) {
      throw new SyncException("Token for zoom not valid");
    }
    $users = $parseUsers->users;

    $msg = 'Count users to fetch meetings: %count';
    $this->wrapper->logger->notice($msg, ['%count' => count($users)]);

    // Get meetings list by users.
    $meetings = [];
    foreach ($users as $user) {
      // @TODO implement pager.
      $requestMeetingsList = 'users/';
      $requestMeetingsList .= $user->email;
      $requestMeetingsList .= '/meetings';
      $requestMeetingsList .= '?type=upcoming&page_size=1000&page_number=1';
      $body = $this->client->get($requestMeetingsList);
      $meetingsList = $this->parse($body)->meetings;
      $msg = 'User %user has meetings to fetch: %count';
      $this->wrapper->logger->notice($msg, [
        '%count' => count(array_unique(array_map(function ($o) {
          return $o->id;
        },
        $meetingsList
        ))),
        '%user' => $user->email,
      ]);

      // Get all user meetings data.
      foreach ($meetingsList as $meeting) {
        if (isset($meetings[$meeting->id])) {
          continue;
        }
        $requestMeeting = 'meetings/' . $meeting->id;
        $bodyMeeting = $this->client->get($requestMeeting);
        $meetings[$meeting->id] = $this->parse($bodyMeeting);
      }
    }
    $msg = 'Finished zoom af sync fetch. count zoom meetings: %count';
    $this->wrapper->logger->notice($msg, ['%count' => count($meetings)]);
    $this->wrapper->addProducts($meetings);
  }

  /**
   * Parse response from Zoom.
   *
   * @param array $response
   *   Parameter array.
   *
   * @return false|object
   *   Object.
   */
  private function parse(array $response) {
    // @TODO create exceptions on errors.
    if ($response['errors']) {
      return FALSE;
    }

    $json = json_decode($response['response']);
    if (!$json) {
      return FALSE;
    }

    // Case for invalid token.
    // `124` - zoom's internal code for invalid token.
    if (isset($json->code) && isset($json->message) && $json->code == 124) {
      $this->wrapper->logger->error('Looks like Zoom API token is expired. Contact BT. API message: %msg', ['%msg' => $json->message]);
      return FALSE;
    }

    return $json;
  }

}
